"""woocommerce tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_woocommerce.streams import OrderStream, ProductStream, RefundStream

STREAM_TYPES = [OrderStream, RefundStream, ProductStream]


class Tapwoocommerce(Tap):
    """woocommerce tap class."""

    name = "tap-woocommerce"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "consumer_key",
            th.StringType,
            required=True,
            description="The woocommerce API consumer key",
        ),
        th.Property(
            "consumer_secret",
            th.StringType,
            required=True,
            description="The woocommerce API consumer secret",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
        th.Property(
            "api_url",
            th.StringType,
            required=True,
            description="The url for the API service",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]
