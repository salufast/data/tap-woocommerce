"""Stream type classes for tap-woocommerce."""

# JSON Schema typing helpers
from typing import Iterable, Optional
import requests
from singer_sdk import typing as th
from tap_woocommerce import typing as custom_th

from tap_woocommerce.client import SCHEMAS_DIR, woocommerceStream


class OrderStream(woocommerceStream):
    """Woocommerce orders endpoint."""

    name = "order"
    path = "/orders"
    primary_keys = ["id"]
    replication_key = "date_modified"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("parent_id", th.IntegerType),
        th.Property("number", th.StringType),
        th.Property("order_key", th.StringType),
        th.Property("created_via", th.StringType),
        th.Property("version", th.StringType),
        th.Property("status", th.StringType),
        th.Property("currency", th.StringType),
        th.Property("currency_symbol", th.StringType),
        th.Property("date_created", th.DateTimeType),
        th.Property("date_created_gmt", th.DateTimeType),
        th.Property("date_modified", th.DateTimeType),
        th.Property("date_modified_gmt", th.DateTimeType),
        th.Property("discount_total", th.StringType),
        th.Property("discount_tax", th.StringType),
        th.Property("shipping_total", th.StringType),
        th.Property("shipping_tax", th.StringType),
        th.Property("shipments", th.ArrayType(custom_th.ShipmentType)),
        th.Property("parcel_delivery_opted_in", th.BooleanType),
        th.Property("cart_tax", th.StringType),
        th.Property("total", th.StringType),
        th.Property("total_tax", th.StringType),
        th.Property("prices_include_tax", th.BooleanType),
        th.Property("customer_id", th.IntegerType),
        th.Property("customer_ip_address", th.StringType),
        th.Property("customer_user_agent", th.StringType),
        th.Property("customer_note", th.StringType),
        th.Property("billing", custom_th.AddressType),
        th.Property("shipping", custom_th.AddressType),
        th.Property("payment_method", th.StringType),
        th.Property("payment_method_title", th.StringType),
        th.Property("direct_debit", custom_th.DirectDebitType),
        th.Property("transaction_id", th.StringType),
        th.Property("date_paid", th.DateTimeType),
        th.Property("date_paid_gmt", th.DateTimeType),
        th.Property("date_completed", th.DateTimeType),
        th.Property("date_completed_gmt", th.DateTimeType),
        th.Property("cart_hash", th.StringType),
        th.Property("meta_data", th.ArrayType(custom_th.MetaDataType)),
        th.Property("line_items", th.ArrayType(custom_th.LineItemType)),
        th.Property("tax_lines", th.ArrayType(custom_th.TaxLineType)),
        th.Property("shipping_lines", th.ArrayType(custom_th.ShippingLineType)),
        th.Property("fee_lines", th.ArrayType(custom_th.FeeLineType)),
        th.Property("coupon_lines", th.ArrayType(custom_th.CouponLineType)),
        th.Property("refunds", th.ArrayType(custom_th.RefundLineType)),
        th.Property("set_paid", th.BooleanType),
        th.Property("_links", custom_th.LinkType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {"order_id": record["id"], "refunds": len(record["refunds"])}

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        for order in super().parse_response(response):
            for line_item in order["line_items"]:
                # bundled_by should be an integer. For some reason,
                # instead of null, the api returns an empty string
                if line_item["bundled_by"] == "":
                    line_item["bundled_by"] = None
            yield order


class RefundStream(woocommerceStream):
    """Woocommerce orders/{order_id}/refunds endpoint"""

    name = "refund"
    path = "/orders/{order_id}/refunds"
    parent_stream_type = OrderStream
    primary_keys = ["id"]
    # important: otherwise, for each order id a state bookmark will be created
    state_partitioning_keys = []
    schema_filepath = SCHEMAS_DIR / "refunds.json"

    def request_records(self, context: Optional[dict]) -> Iterable[dict]:
        if context["refunds"] == 0:
            return []
        return super().request_records(context)


class ProductStream(woocommerceStream):
    """Woocommerce products endpoint"""

    name = "product"
    path = "/products"
    primary_keys = ["id"]
    replication_key = "date_modified"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("slug", th.StringType),
        th.Property("permalink", th.StringType),
        th.Property("date_created", th.DateTimeType),
        th.Property("date_created_gmt", th.DateTimeType),
        th.Property("date_modified", th.DateTimeType),
        th.Property("date_modified_gmt", th.DateTimeType),
        th.Property("type", th.StringType),
        th.Property("status", th.StringType),
        th.Property("featured", th.BooleanType),
        th.Property("catalog_visibility", th.StringType),
        th.Property("description", th.StringType),
        th.Property("short_description", th.StringType),
        th.Property("mini_desc", th.StringType),
        th.Property("sku", th.StringType),
        th.Property("price", th.StringType),
        th.Property("regular_price", th.StringType),
        th.Property("sale_price", th.StringType),
        th.Property("date_on_sale_from", th.DateTimeType),
        th.Property("date_on_sale_from_gmt", th.DateTimeType),
        th.Property("date_on_sale_to", th.DateTimeType),
        th.Property("date_on_sale_to_gmt", th.DateTimeType),
        th.Property("price_html", th.StringType),
        th.Property("on_sale", th.BooleanType),
        th.Property("purchasable", th.BooleanType),
        th.Property("total_sales", th.IntegerType),
        th.Property("virtual", th.BooleanType),
        th.Property("downloadable", th.BooleanType),
        th.Property("downloads", th.ArrayType(custom_th.DownloadType)),
        th.Property("download_limit", th.IntegerType),
        th.Property("download_expiry", th.IntegerType),
        th.Property("external_url", th.StringType),
        th.Property("button_text", th.StringType),
        th.Property("tax_status", th.StringType),
        th.Property("tax_class", th.StringType),
        th.Property("manage_stock", th.BooleanType),
        th.Property("stock_quantity", th.IntegerType),
        th.Property("stock_status", th.StringType),
        th.Property("low_stock_amount", th.BooleanType),
        th.Property("backorders", th.StringType),
        th.Property("backorders_allowed", th.BooleanType),
        th.Property("backordered", th.BooleanType),
        th.Property("sold_individually", th.BooleanType),
        th.Property("weight", th.StringType),
        th.Property("dimensions", custom_th.DimensionType),
        th.Property("shipping_required", th.BooleanType),
        th.Property("shipping_taxable", th.BooleanType),
        th.Property("shipping_class", th.StringType),
        th.Property("shipping_class_id", th.IntegerType),
        th.Property("reviews_allowed", th.BooleanType),
        th.Property("average_rating", th.StringType),
        th.Property("rating_count", th.IntegerType),
        th.Property("related_ids", th.ArrayType(th.IntegerType)),
        th.Property("upsell_ids", th.ArrayType(th.IntegerType)),
        th.Property("cross_sell_ids", th.ArrayType(th.IntegerType)),
        th.Property("parent_id", th.IntegerType),
        th.Property("purchase_note", th.StringType),
        th.Property("categories", th.ArrayType(custom_th.CategoryType)),
        th.Property("tags", th.ArrayType(custom_th.TagType)),
        th.Property("unit", custom_th.UnitType),
        th.Property("unit_price", custom_th.UnitPriceType),
        th.Property("min_age", th.StringType),
        th.Property("sale_price_label", custom_th.SalePriceLabelType),
        th.Property("sale_price_regular_label", custom_th.SalePriceLabelType),
        th.Property("free_shipping", th.BooleanType),
        th.Property("service", th.BooleanType),
        th.Property("differential_taxation", th.BooleanType),
        th.Property("images", th.ArrayType(custom_th.ImageType)),
        th.Property("attributes", th.ArrayType(custom_th.AttributeType)),
        th.Property("default_attributes", th.ArrayType(custom_th.DefaultAttributeType)),
        th.Property("variations", th.ArrayType(th.IntegerType)),
        th.Property("grouped_products", th.ArrayType(th.IntegerType)),
        th.Property("menu_order", th.IntegerType),
        th.Property("meta_data", th.ArrayType(custom_th.MetaDataType)),
        th.Property("_links", custom_th.LinkType),
        th.Property("bundled_by", th.ArrayType(th.StringType)),
        th.Property("bundle_virtual", th.BooleanType),
        th.Property("bundle_layout", th.StringType),
        th.Property("bundle_add_to_cart_form_location", th.StringType),
        th.Property("bundle_editable_in_cart", th.BooleanType),
        th.Property("bundle_sold_individually_context", th.StringType),
        th.Property("bundle_item_grouping", th.StringType),
        th.Property("bundle_min_size", th.IntegerType),
        th.Property("bundle_max_size", th.IntegerType),
        th.Property("bundle_stock_status", th.StringType),
        th.Property("bundled_items", th.ArrayType(custom_th.BundledItemType)),
        th.Property("bundle_stock_quantity", th.IntegerType),
    ).to_dict()

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        for product in super().parse_response(response):
            # unit should be an object. For some reason,
            # instead of null, the api returns an empty list
            if product["unit"] == []:
                product["unit"] = None

            # sale_price_label and sale_price_regular_label should be objects.
            # For some reason, instead of null, the api returns an empty list
            if product["sale_price_label"] == []:
                product["sale_price_label"] = None
            if product["sale_price_regular_label"] == []:
                product["sale_price_regular_label"] = None

            # bundle_min_size, bundle_max_size, bundle_stock_quantity should be
            # integers. For some reason, instead of null, the api returns an
            # empty string
            if product["bundle_min_size"] == "":
                product["bundle_min_size"] = None
            if product["bundle_max_size"] == "":
                product["bundle_max_size"] = None
            if product["bundle_stock_quantity"] == "":
                product["bundle_stock_quantity"] = None
            yield product
