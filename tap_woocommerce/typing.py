from singer_sdk.typing import (
    CustomType,
    DateTimeType,
    Property,
    StringType,
    IntegerType,
    ArrayType,
    PropertiesList,
    BooleanType,
    NumberType,
)

AddressType = PropertiesList(
    Property("first_name", StringType),
    Property("last_name", StringType),
    Property("company", StringType),
    Property("address_1", StringType),
    Property("address_2", StringType),
    Property("city", StringType),
    Property("state", StringType),
    Property("postcode", StringType),
    Property("country", StringType),
    Property("email", StringType),
    Property("phone", StringType),
)

MetaDataType = CustomType({"type": "object"})

TaxType = PropertiesList(
    Property("id", IntegerType),
    Property("rate_code", StringType),
    Property("rate_id", StringType),
    Property("label", StringType),
    Property("compound", BooleanType),
    Property("tax_total", StringType),
    Property("shipping_tax_total", StringType),
    Property("total", StringType),
    Property("subtotal", StringType),
    Property("refund_total", NumberType),
    Property("meta_data", ArrayType(MetaDataType)),
)

TaxLineType = PropertiesList(
    Property("id", IntegerType),
    Property("rate_code", StringType),
    Property("rate_id", IntegerType),
    Property("label", StringType),
    Property("compound", BooleanType),
    Property("tax_total", StringType),
    Property("shipping_tax_total", StringType),
    Property("meta_data", ArrayType(MetaDataType)),
)

ShippingLineType = PropertiesList(
    Property("id", IntegerType),
    Property("method_title", StringType),
    Property("method_id", StringType),
    Property("total", StringType),
    Property("total_tax", StringType),
    Property("taxes", ArrayType(TaxType)),
    Property("meta_data", ArrayType(MetaDataType)),
)

FeeLineType = PropertiesList(
    Property("id", IntegerType),
    Property("name", StringType),
    Property("tax_class", StringType),
    Property("tax_status", StringType),
    Property("total", StringType),
    Property("total_tax", StringType),
    Property("taxes", ArrayType(TaxType)),
    Property("meta_data", ArrayType(MetaDataType)),
)

CouponLineType = PropertiesList(
    Property("id", IntegerType),
    Property("code", StringType),
    Property("discount", StringType),
    Property("discount_tax", StringType),
    Property("meta_data", ArrayType(MetaDataType)),
)

RefundLineType = PropertiesList(
    Property("id", IntegerType),
    Property("refund", StringType),
    Property("total", StringType),
)

BundleConfigurationAttributeType = PropertiesList(
    Property("name", StringType), Property("option", StringType)
)

BundleConfigurationType = PropertiesList(
    Property("bundled_item_id", IntegerType),
    Property("product_id", IntegerType),
    Property("quantity", IntegerType),
    Property("title", StringType),
    Property("optional_selected", BooleanType),
    Property("variation_id", IntegerType),
    Property("attributes", ArrayType(BundleConfigurationAttributeType)),
    Property("args", CustomType({"type": "object"})),
)

DimensionType = PropertiesList(
    Property("length", StringType),
    Property("width", StringType),
    Property("height", StringType),
)

LineItemType = PropertiesList(
    Property("id", IntegerType),
    Property("name", StringType),
    Property("product_id", IntegerType),
    Property("variation_id", IntegerType),
    Property("quantity", IntegerType),
    Property("tax_class", StringType),
    Property("subtotal", StringType),
    Property("subtotal_tax", StringType),
    Property("total", StringType),
    Property("total_tax", StringType),
    Property("taxes", ArrayType(TaxType)),
    Property("meta_data", ArrayType(MetaDataType)),
    Property("sku", StringType),
    Property("price", NumberType),
    Property("refund_total", NumberType),
    Property("bundled_by", IntegerType),
    Property("bundled_items", ArrayType(IntegerType)),
    Property("bundled_item_title", StringType),
    Property("bundle_configuration", ArrayType(BundleConfigurationType)),
)

ShipmentItemType = PropertiesList(
    Property("id", IntegerType),
    Property("name", StringType),
    Property("order_item_id", IntegerType),
    Property("product_id", IntegerType),
    Property("quantity", IntegerType),
)

ShipmentType = PropertiesList(
    Property("id", IntegerType),
    Property("date_created", DateTimeType),
    Property("date_created_gmt", DateTimeType),
    Property("date_sent", DateTimeType),
    Property("date_sent_gmt", DateTimeType),
    Property("est_delivery_date", DateTimeType),
    Property("est_delivery_date_gmt", DateTimeType),
    Property("total", StringType),
    Property("weight", StringType),
    Property("status", StringType),
    Property("tracking_id", StringType),
    Property("tracking_url", StringType),
    Property("shipping_provider", StringType),
    Property("dimensions", DimensionType),
    Property("address", AddressType),
    Property("items", ArrayType(ShipmentItemType)),
)

DirectDebitType = PropertiesList(
    Property("holder", StringType),
    Property("iban", StringType),
    Property("bic", StringType),
    Property("mandate_id", StringType),
)

LinkType = PropertiesList(Property("href", StringType))

ReferencesType = PropertiesList(
    Property("self", ArrayType(LinkType)), Property("collection", ArrayType(LinkType))
)

DownloadType = PropertiesList(
    Property("id", StringType),
    Property("name", StringType),
    Property("file", StringType),
)

CategoryType = PropertiesList(
    Property("id", IntegerType),
    Property("name", StringType),
    Property("slug", StringType),
)

TagType = CategoryType

UnitType = CategoryType

SalePriceLabelType = CategoryType

UnitPriceType = PropertiesList(
    Property("base", StringType),
    Property("product", StringType),
    Property("price_auto", BooleanType),
    Property("price", StringType),
    Property("price_regular", StringType),
    Property("price_sale", StringType),
    Property("price_html", StringType),
)

ImageType = PropertiesList(
    Property("id", IntegerType),
    Property("date_created", DateTimeType),
    Property("date_created_gmt", DateTimeType),
    Property("date_modified", DateTimeType),
    Property("date_modified_gmt", DateTimeType),
    Property("src", StringType),
    Property("name", StringType),
    Property("alt", StringType),
)

AttributeType = PropertiesList(
    Property("id", IntegerType),
    Property("name", StringType),
    Property("position", IntegerType),
    Property("visible", BooleanType),
    Property("options", ArrayType(StringType)),
)

DefaultAttributeType = PropertiesList(
    Property("id", IntegerType),
    Property("name", StringType),
    Property("option", StringType),
)

BundledItemType = PropertiesList(
    Property("id", IntegerType),
    Property("delete", BooleanType),
    Property("product_id", IntegerType),
    Property("menu_order", IntegerType),
    Property("quantity_min", IntegerType),
    Property("quantity_max", IntegerType),
    Property("priced_individually", BooleanType),
    Property("shipped_individually", BooleanType),
    Property("override_title", BooleanType),
    Property("title", StringType),
    Property("override_description", BooleanType),
    Property("description", StringType),
    Property("optional", BooleanType),
    Property("hide_thumbnail", BooleanType),
    Property("discount", StringType),
    Property("override_variations", BooleanType),
    Property("allowed_variations", ArrayType(IntegerType)),
    Property("override_default_variation_attributes", BooleanType),
    Property("default_variation_attributes", ArrayType(DefaultAttributeType)),
    Property("single_product_visibility", StringType),
    Property("cart_visibility", StringType),
    Property("single_product_visibility", StringType),
    Property("order_visibility", StringType),
    Property("cart_price_visbility", StringType),
    Property("stock_status", StringType),
)
